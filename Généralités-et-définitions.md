## Les événements ##

Les événements sont gérés de manière hiérarchique.

-------------------------------------------------------------------------
*__Exemple__*
	Un événement sportif peut comporter plusieurs éditions (événement récurrent). Chaque édition peut elle même comporter plusieurs épreuves.

-------------------------------------------------------------------------
Une photo appartient à un unique événement.
Un tag appartient à un unique événement.
Une photo peut comporter plusieurs tags, n'appartenant pas nécessairement au même événement que la photo.

-------------------------------------------------------------------------
*__Exemple__*
Un évènement sportif X comporte une édition 2016. Cette édition comporte deux courses (marathon et semi-marathon). Les dossards sont uniques pour l'édition 2016.
	Les photos seront transférées sur l'événement Édition 2016. Lors du tagging, nous pourrons apposer des tags de la course marathon ou semi-marathon.

-------------------------------------------------------------------------

Lors d'un transfert de photos, il faut donc s'assurer de l'unicité du paramètre identifiant le participant (le dossard dans le cadre d'un événement sportif).

## Les utilisateurs ##

Il y a deux types d'utilisateurs :

* Les administrateurs : ils accèdent à l'interface d'administration. Ils peuvent avoir un ou plusieurs rôles :
	* Administrateur
	* Photographe
	* Tagger
* Les utilisateurs : Ils participent à un ou plusieurs événements, et peuvent acheter / télécharger leurs photos. Ils accédent à un compte personnel sur le site public WebPhotoLive.