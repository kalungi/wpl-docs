# Page tarifs boutique (plugin Wordpress)
Deux méthodes d'implémentation de la page tarifs sont possibles
## Page générée et non modifiable
Utiliser le shortcode sans arguments ni contenu :
```
[wplshop_rates]
```