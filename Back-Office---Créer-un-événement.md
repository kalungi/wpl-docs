1. Choisir la rubrique ''Evenements'' dans le menu.
![event_menu.png](images/event_menu.png)
2. Remplir le formulaire "Creer un nouvel événement". Pour créer un événement racine, laisser le sélecteur "sous-événement de" vide.
![event_create_form.png](images/event_create_form.png)
3. Valider