# Ajouter des utilisateurs

## Par import

Documentation à venir

## Par formulaire

### Ajouter un utilisateur

Se rendre de le menu Utilisateurs enregistrés

![user_menu.png](https://bitbucket.org/repo/j4pjb9/images/26570657-user_menu.png)

Cliquer sur le bouton "Ajouter un utilisateur" en bas de liste :

![user_add_button.png](https://bitbucket.org/repo/j4pjb9/images/945007195-user_add_button.png)

Remplir le formulaire d'ajout :

![user_add_form.png](https://bitbucket.org/repo/j4pjb9/images/2553756475-user_add_form.png)

Vous êtes redirigés vers la page du profil de l'utilisateur nouvellement créé :

![user_show.png](https://bitbucket.org/repo/j4pjb9/images/1494660617-user_show.png)

### Ajouter une participation à un événement

Sur la page de profil de l'utilisateur, cliquer sur le bouton "Ajouter une participation" :

![participation_add_button.png](https://bitbucket.org/repo/j4pjb9/images/2190744133-participation_add_button.png)

Une fenêtre modale s'ouvre :

![sportingparticipation_add_button.png](https://bitbucket.org/repo/j4pjb9/images/694916082-sportingparticipation_add_button.png)

Pour le moment, seuls les événements sportifs sont supportés.

Cliquer sur Ajouter une participation à un événement sportif.

Replir le formulaire et valider :

![sportingparticipation_add_form.png](https://bitbucket.org/repo/j4pjb9/images/2178663717-sportingparticipation_add_form.png)

Vous êtes redirigés vers la page de profil de l'utilisateur, et la participation a bien été prise en compte :

![user_show_with_participation.png](https://bitbucket.org/repo/j4pjb9/images/3597902405-user_show_with_participation.png)