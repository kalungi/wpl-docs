Deux méthodes d'implémentation de la page tarifs sont possibles
## Page générée et non modifiable
Utiliser le shortcode sans arguments ni contenu :
```
[wplshop_rates]
```
## Contenu personnalisé
Utiliser le même shortcode et placer le contenu de la page entre la balise ouvrante et la balise fermante. Par exemple :
```html
[wplshop_rates]
<h2>Photo numérique</h2>
<strong>Pour _% jpeg.price %_</strong> €*, votre photo en format haute définition (de 6,7 à 9,6 millions de pixels) (fichier jpg).
* Tarif appliqué par photo sélectionnée (paiement CB ou chèque)
<h2>Tirages Photographiques</h2>
<table>
	<thead>
		<tr>
			<th>Articles</th>
			<th>Prix unitaire 1er tirage</th>
			<th>Prix unitaire à partir de 2 tirages</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>_% tirage-13x19.product.title %_</th>
			<td>_% tirage-13x19.price %_ €</td>
			<td>_% tirage-13x19.price %_ €</td>
		</tr>
		<tr>
			<th>_% tirage-15x21.product.title %_</th>
			<td>_% tirage-15x21.price %_ €</td>
			<td>_% tirage-15x21.price %_ €</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3">Papier photo professionnel brillant de marque Fujifilm Crystal Suprême</td>
		</tr>
	</tfoot>
</table>
[/wplshop_rates]
```
Le contenu positionné entre `_%` et `%_` sera modifié avec les valeurs correspondantes (variables magiques). Ces variables magiques sont composées de la manière suivante :
```<product slug>.<product property>```
Les valeurs suivantes sont possibles pour `<product property>` :
* `price` : Le prix hors taxe pour l'événement courant
* `vat` : La TVA (valeur comprise entre 0 et 1)
* `incltaxPrice` : Le prix TTC
* `product.title` : Le nom du produit
* `product.slug` : La référence du produit
* `product.description` : La description du produit
* `product.group` : Le groupe auquel appartient le produit

---------------------------------------------------------------
*__Exemple__* :
Pour afficher la description du produit dont la référence est `test`, on utilisera la syntaxe suivante :
```_% test.product.description %_```

----------------------------------------------------------------