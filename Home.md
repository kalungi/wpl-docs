# Documentation WebPhotoLive
## Table des matières
* [Généralités et définitions](Généralités-et-définitions)

### Administration
* [Procédures](https://bitbucket.org/kalungi/wpl-admin/wiki/process)
	* [Créer un événement](Back-Office---Créer-un-événement)
	* [Ajouter des utilisateurs](Back-Office---Ajouter-des-utilisateurs)

### Boutique en ligne (Plugin Wordpress)
* [Page tarifs](Boutique-Wordpress---Page-tarifs)